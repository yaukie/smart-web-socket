package com.yaukie.socket.controller;


import com.yaukie.socket.constant.BaseResult;
import com.yaukie.socket.constant.BaseResultConstant;
import com.yaukie.socket.server.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController    {

    @Autowired
    private WebSocketServer webSocketServer ;

    @PostMapping("/sendSocket/{topicId}")
    @ResponseBody
      public Object sendSocket(
            @PathVariable(value = "topicId") String topicId
      ) {
        String msg = "测试消息" ;
        webSocketServer.sendMessageToChannel(topicId,msg);
        return new BaseResult(BaseResultConstant.SUCCESS,msg) ;
    }

}
