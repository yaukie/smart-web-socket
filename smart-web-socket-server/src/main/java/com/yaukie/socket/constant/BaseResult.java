package com.yaukie.socket.constant;

 
/**
  * @功能名称：BaseResult
  * @功能描述：TODO
  * @作者：yuenbin
  * @创建时间：2022/3/6 12:53
  * @Motto：It is better to be clear than to be clever !
**/
public class BaseResult<T> {
     public int code;
     public String message;
     public T data;

    public BaseResult() {
        this.code = BaseResultConstant.SUCCESS.code;
        this.message = BaseResultConstant.SUCCESS.message;
        this.data = null;
    }

    public BaseResult(int code, String message) {
        this(code, message, null);
    }

    public BaseResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;

    }


    public BaseResult(BaseResultConstant ocResultConstant, T data) {
        this(ocResultConstant.code, ocResultConstant.message, data);
    }

    public void setApiResultConstant(BaseResultConstant ocResultConstant) {
        this.code = ocResultConstant.code;
        this.message = ocResultConstant.message;
     }

    public void setApiResultConstant(int code, String message) {
        this.code = code;
        this.message = message;
     }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
     }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public String toString() {
        return "ApiResult{code=" + this.code + ", message='" + this.message + '\'' + ", data=" + this.data + ", success"+'}';
    }
}
