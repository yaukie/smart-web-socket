//package com.yaukie.socket.config;
//
//import com.yaukie.socket.interceptor.WebSocketHandlerInterceptor;
//import com.yaukie.socket.server.SpringWebsocketHandler;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//import org.springframework.web.socket.config.annotation.EnableWebSocket;
//import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
//import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
//
//@Component
//@EnableWebMvc
//@EnableWebSocket
//public class WebSocketConfiguration extends WebMvcConfigurerAdapter implements WebSocketConfigurer {
//
//    @Override
//    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
//        webSocketHandlerRegistry.addHandler(websocketHandler(),"/websocket/socket")
//                .addInterceptors(new WebSocketHandlerInterceptor())
//                .setAllowedOrigins("*") ;
//
//        webSocketHandlerRegistry.addHandler(websocketHandler(),"/sockjs/socket")
//                .addInterceptors(new WebSocketHandlerInterceptor())
//                .setAllowedOrigins("*") .withSockJS();
//    }
//
//    @Bean
//    public SpringWebsocketHandler websocketHandler(){
//        return new SpringWebsocketHandler() ;
//    }
//
//
//}
