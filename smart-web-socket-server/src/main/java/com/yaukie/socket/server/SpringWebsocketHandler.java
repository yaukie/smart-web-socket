//package com.yaukie.socket.server;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import Constants;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//import org.springframework.web.socket.CloseStatus;
//import org.springframework.web.socket.TextMessage;
//import org.springframework.web.socket.WebSocketSession;
//import org.springframework.web.socket.handler.TextWebSocketHandler;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.atomic.AtomicInteger;
//
//  public class SpringWebsocketHandler extends TextWebSocketHandler {
//
//    private static final long sessionTimeout = 600000;
//
//    private static final Logger log = LoggerFactory.getLogger(SpringWebsocketHandler.class);
//
//    /**
//     *  用于缓存websocket服务器
//     */
//    private static Map<String,SpringWebsocketHandler> WEB_SOCKET_CACHE = new ConcurrentHashMap<>() ;
//
//    private WebSocketSession  session ;
//
//
//    private String topicId ;
//
//    private static AtomicInteger WEB_SOCKET_LINE = new AtomicInteger(0) ;
//
//
//
//    @Autowired
//    private RedisTemplate redisTemplate ;
//
//
//
//
//    /**
//     * @Description： 建立连接的时候触发
//     * @params：
//     * @return :
//     * @author : yuenbin
//     * @date： 14:45 2023/7/24
//     * @Motto： It is better to be clear than to be clever !
//     **/
//    @Override
//    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
//         this.session.setTextMessageSizeLimit(10000);
//        this.session = session ;
//        this.topicId= this.getTopicId(session) ;
//        if(WEB_SOCKET_CACHE.containsKey(topicId)){
//            WEB_SOCKET_CACHE.remove(topicId) ;
//            // 更新当前服务器消息内容
//            WEB_SOCKET_CACHE.put(topicId,this);
//        }else {
//            WEB_SOCKET_CACHE.put(topicId,this);
//            WEB_SOCKET_LINE.getAndIncrement() ;
//        }
//        try {
//            this.sendMsg("服务器-"+ InetAddress.getLocalHost().getCanonicalHostName()+""+"-连接成功！");
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    /**
//     * @Description： 关闭连接的时候触发
//     * @params：
//     * @return :
//     * @author : yuenbin
//     * @date： 14:45 2023/7/24
//     * @Motto： It is better to be clear than to be clever !
//     **/
//    @Override
//    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
//        if(WEB_SOCKET_CACHE.containsKey(topicId)){
//            WEB_SOCKET_CACHE.remove(topicId) ;
//            WEB_SOCKET_LINE.getAndDecrement() ;
//            try {
//                log.info("服务器-{}-退出连接！",InetAddress.getLocalHost().getCanonicalHostName(),topicId);
//                log.info("当前服务器消息主题在线个数为-{}！",WEB_SOCKET_LINE.get());
//            } catch (UnknownHostException e) {
//                e.printStackTrace();
//            }
//
//        }
//    }
//
//    /**
//     * @Description： 出现异常的时候触发
//     * @params：
//     * @return :
//     * @author : yuenbin
//     * @date： 14:45 2023/7/24
//     * @Motto： It is better to be clear than to be clever !
//     **/
//    @Override
//    public void handleTransportError(WebSocketSession session, Throwable e) throws Exception {
//        log.error("编号id错误:" + this.topicId + ",原因:" + e.getMessage());
//        e.printStackTrace();
//    }
//
//    /**
//     * @Description： 接收到客户端消息的时候触发
//     * @params：
//     * @return :
//     * @author : yuenbin
//     * @date： 14:45 2023/7/24
//     * @Motto： It is better to be clear than to be clever !
//     **/
//    @Override
//    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
//        String payload = message.getPayload();
//        JSONObject jsonObject = JSON.parseObject(payload) ;
//        this.sendMessageToChannel(jsonObject.getString(Constants.SOCKET_REDIS_KEY),jsonObject.getString(Constants.SOCKET_REDIS_VAL));
//        log.info("发送主题为-{}-的消息，消息报文为：{}",topicId,message,null,null,null);
//    }
//
//
//    /**
//     * @Description： 发布消息，需要指定redis频道
//     * @params：
//     * @return :
//     * @author : yuenbin
//     * @date： 10:50 2023/7/22
//     * @Motto： It is better to be clear than to be clever !
//     **/
//    public void  sendMessageToChannel(String topicId,String msg ){
//        String message = null ;
//        try {
//            message = new String(msg.getBytes("UTF-8"),"UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//        Map<String ,String> msgObj = new ConcurrentHashMap<>() ;
//        msgObj.put(Constants.SOCKET_REDIS_KEY,topicId);
//        msgObj.put(Constants.SOCKET_REDIS_VAL,message);
//        redisTemplate.convertAndSend(Constants.REDIS_CHANNEL_ID, JSON.toJSONString(msgObj));
//    }
//
//    /**
//     * @Description： 根据订阅频道发送消息，必须指定频道ID
//     * @params：
//     * @return :
//     * @author : yuenbin
//     * @date： 11:37 2023/7/22
//     * @Motto： It is better to be clear than to be clever !
//     **/
//    public void sendMsgByTopicId( String topicId,String msg ){
//        SpringWebsocketHandler webSocketHandler = null ;
//        if(WEB_SOCKET_CACHE.containsKey(topicId)){
//            webSocketHandler = WEB_SOCKET_CACHE.get(topicId) ;
//            if(!StringUtils.isEmpty(webSocketHandler) ){
//                webSocketHandler.sendMsg(msg);
//                log.error("消息由服务器-{}-发出",topicId);
//            }
//        }else {
//            log.error("消息服务器-{}-已下线或不存在，因此消息发发送失败");
//        }
//
//    }
//
//    /**
//     * @Description： 使用websocket发送消息
//     * @params：
//     * @return :
//     * @author : yuenbin
//     * @date： 11:41 2023/7/22
//     * @Motto： It is better to be clear than to be clever !
//     **/
//    public void sendMsg(String msg ){
//
//        try {
//            this.session.sendMessage(new TextMessage(msg));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private String getTopicId (WebSocketSession session ){
//
//        String  topicId =(String) session.getAttributes().get("topicId");
//        return topicId ;
//
//    }
//
//}
