//package com.yaukie.socket.interceptor;
//
//import org.apache.log4j.Logger;
//import org.springframework.http.server.ServerHttpRequest;
//import org.springframework.http.server.ServerHttpResponse;
//import org.springframework.http.server.ServletServerHttpRequest;
//import org.springframework.web.socket.WebSocketHandler;
//import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;
//
//import javax.servlet.http.HttpSession;
//import java.util.Map;
//
///**
// * @功能名称： 使用另一种写法实现websocket ，
// * @功能描述：
// * @作者： yuenbin
// * @创建时间： 14:36 2023/7/24
// * @Motto： It is better to be clear than to be clever !
// **/
//public class WebSocketHandlerInterceptor  extends HttpSessionHandshakeInterceptor {
//
//    private static final Logger log = Logger.getLogger(WebSocketHandlerInterceptor.class);
//
//    @Override
//    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
//         ServletServerHttpRequest  req= (ServletServerHttpRequest) request;
//        String topicId = req.getServletRequest().getParameter("topicId");
//        HttpSession session = req.getServletRequest().getSession();
//        if(session !=null ){
//            attributes.put("topicId",topicId) ;
//        }
//        return super.beforeHandshake(request, response, wsHandler, attributes);
//    }
//
//    @Override
//    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception ex) {
//        super.afterHandshake(request, response, wsHandler, ex);
//    }
//}
