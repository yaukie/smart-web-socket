package com.yaukie.socket.constant;

public interface Constants {

    public static final String REDIS_CHANNEL_ID = "REDIS_CHANNEL" ;

    public static final String SOCKET_REDIS_KEY = "S_TOPIC" ;

        public static final String SOCKET_REDIS_VAL = "S_MSG" ;

}
