# Smart Websocket（全双工通信机制）
 
## 引言 
    
    本项目是基于Springboot+Websocket实现的企业业务多端之间实时通信、消息跨节点、跨集群共享的全双工通信系统，
    解决了企业业务多终端通信无法保持长连接、消息发送失败、多节点之间消息无法共享、无法订阅的问题，提升通信效率。
    
    Websocket是html5新增的全双工通讯协议，基于Tcp协议，属于应用层协议    浏览器和服务器只需要一次握手就可以创
    建持久性的连接，并进行双向数据传输，也是解决了半双工通讯协议的弊端。
    简单来说就是客户端可以向服务器主动推送信息，服务器也可以主动向客户端主动推送协议。
    
    基于上述Websocket的独有的天然优势，外加当前互联网行业较为流行的H5技术，特此打造了一款实时通信机制系统。
    
## 企业痛点  
 - HTTP协议无法保持长连接，导致频繁建立握手连接耗费资源、成本较高，瓶颈明显  
 - HTTP无状态，客户端消息发送完即断开连接，无法保证准确性、及时性  
 - HTTP请求是一种被动连接、被动请求/响应，无法准确探测服务端通信状态  
 - HTTP存在跨域风险，解决跨域问题需要限制同源策略  
 - HTTP是一种单向通信，不支持全双工双向通信  
     
## Websocket优势

### （1）架构方面的优势  

   Websocket可以解决传统HTTP协议无法满足的实时通信问题，具体来说，可以解决以下几个问题：  
-  实时通信  
        WebSocket可以通过简单易用的API支持实时双向通信，使得Web应用程序可以像
        传统客户端应用程序一样快速响应用户的操作，例如在线聊天、多人游戏等。
 -   高并发处理  
        传统的HTTP协议每次请求都需要建立一个新的连接，而WebSocket协议可以在一个TCP连接上进行多次请求
        和响应，减少了建立连接的时间和网络资源的消耗，从而提高了服务器的处理效率和吞吐量。
  - 大规模消息推送  
      WebSocket协议可以轻松地实现大规模的消息推送，例如实时股票行情、   即时新闻更新等，避免了使用轮询等方式带来的性能问题和带宽浪费。
   - 跨平台兼容性  
      WebSocket技术是跨平台的，不仅支持浏览器客户端，还支持移动客户端、
     桌面客户端和服务端程序等多种平台，以及各种编程语言的开发。
  - 物联网领域  
     WebSocket技术为物联网领域提供了一种重要的实时通信方式，可以支持设备之间的实时数据交换和控制指令传输等应用场景。

### （2）浏览器方面的优势  
    
   基本上所有的主流浏览器均支持WebSocket：  
  -  Chrome：支持WebSocket，从Chrome 4开始。
  - Firefox：支持WebSocket，从Firefox 4开始。
  - Safari：支持WebSocket，从Safari 5开始。
  - Edge：支持WebSocket，从Edge 12开始。
  - Opera：支持WebSocket，从Opera 11.5开始。
  - iOS Safari：支持WebSocket，从iOS 4.2开始。
  - Android Browser：部分支持WebSocket，从Android 2.3开始。  

### （3）天生方面的优势  
    
   WebSocket和HTTP是两种不同的协议，它们有以下几个主要区别：
    
 -  连接方式  
     HTTP协议是基于请求和响应的模型，每次客户端需要获取数据时都需要发送一个新的HTTP请求，而WebSocket协议则是一种全双工的协议，在客户端和服务器之间建立一次连接后，双方可以随时发送数据。
 - 数据格式  
    HTTP协议传输的数据一般采用JSON、XML等格式进行封装，而WebSocket协议则可以直接传输二进制数据。
 - 通信效率  
     HTTP协议中每次数据传输都需要经过请求和响应的过程，而WebSocket协议则避免了这种重复的过程，可以实现更加高效的实时通信。
 - 安全性  
     HTTP协议的安全性依赖于SSL/TLS协议，而WebSocket协议本身也支持SSL/TLS加密，可以提供更好的安全保障。
    
### （4）跨域方面的优势     
   websocket可以解决跨域造成的客户端与服务器无法通信的问题，因为Websocket协议没有同源策略的限制 
       
## 项目简介
 
 项目后端结构如下：  
    
    ├─src
    │  ├─main
    │  │  ├─java
    │  │  │  └─com
    │  │  │      └─yaukie
    │  │  │          └─socket
    │  │  │              ├─config
    │  │  │              ├─constant
    │  │  │              ├─controller
    │  │  │              ├─interceptor
    │  │  │              ├─listener
    │  │  │              └─server
    │  │  └─resources
    │  └─test
    │      ├─java
    │      │  └─com
    │      │      └─lambo
    │      │          └─auth
    │      └─resources
    │          └─sql
    └─target
        ├─classes
        │  └─com
        │      └─yaukie
        │          └─socket
        │              ├─config
        │              ├─constant
        │              ├─controller
        │              ├─listener
        │              └─server
        ├─generated-sources
        │  └─annotations
        └─test-classes
  项目前端如下：  
     
     1） format.js :  提供一系列的格式化工具，用于接口参数的数据转化  
     2)  index.html:  提供一个用于socket通信机制的样例，便于业务端参考使用 
     3)  socket.js :  提供了用于构建websocket全生命周期的脚本支持，便于企业业务系统直接调用、使用，提高研发效率
           
 
## 环境要求
    Maven3+
    Jdk1.8+
    Mysql5.7+
## 使用步骤

### 步骤一  
  准备三台服务器，定义端口号分别为8080/8081/8082  
  建议使用bat脚本的方式启动，启动过程如下图所示：  
     ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_1.jpg)    
     ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_2.jpg)    
     ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_3.jpg)    

### 步骤二  
   根据步骤一启动三台服务器情况，启动websocket服务器，出现  StartApplication   Started StartApplication in 4.132 seconds  
    表示websocket服务器启动成功    
    访问前端界面，在浏览器输入 http://localhost:8083/b9/websocket/index.html  
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_4.jpg)    
    由于启动了三个websocket服务器，因此需要开启三个前端来进行验证，分别绑定8080/8081/8082这三台服务器  
    点击开启连接，对应的服务器将会自动连接上，如图所示：  
     ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_5.jpg)     
     ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_6.jpg)    
     ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_7.jpg)    
     
   如上图所示，三台websocket服务器都是订阅了同一个消息，即CC，当客户端发送消息到任何一个服务器的时候，尤其是在集群、负载环境中  
   单节点环境无法保证websocket的会话共享，因此也就无法保证消息被其他节点服务器共享，当用户发送消息的时候，  
   会话保存在某一台服务器上，其他服务器将无法获取，这里使用了Redis 作为发布订阅中介者，实现了负载均衡环境下，  
    websocket会话共享的方案  ,给8080服务器发送消息，8081、8082都能从redis订阅到，请参考下图：  
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_8.jpg)    
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_9.jpg)    
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_10.jpg)    
     
   同样的，客户端通过http请求，给8080服务器发送消息，8081/8082服务器也能监听到同一个主题的消息，  执行如下命令发送消息：  
    curl -X POST "http://localhost:8080/sendSocket/CC" -H "accept: */*"  执行情况如下图所示：  
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_11.jpg)    
      
   可以看到在8081/8082服务器都收到了这条消息  ，详见如下图所示：  
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_12.jpg)    
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_13.jpg) 
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_14.jpg)    
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_15.jpg) 
   ![前端镜像](https://gitee.com/yaukie/smart-web-socket/raw/master/folders/socket_16.jpg)    
    
## 后续计划 
功能需求方面  
- 1、当前仅支持到SpringBoot框架层面，后续考虑增加支持SpringWeb框架  
- 2、后续考虑增加用户权限、角色权限设计  
- 3、后续考虑增加线程池高级配置，支持多线程任务的灵活配置  
- 4、后续考虑构建一套完整的SpringBoot+Vue套件样例系统，真正实现完整的Websocket通信系统  
部署运维方面  
- 1、考虑做到应用+服务+数据库一体化，jar包集成  
- 2、考虑通过Docker容器方式实现功能移植  
- 3、考虑通过Jenkins实现打包、部署  
- 4、考虑发布到阿里云或其他什么云，远端访问  
 